#!/bin/bash

usage() {
    echo "Usage: $0  ‹Destination Folder› ‹Canal+.fr URL | '?vid=' Integer› [ ‹Canal+.fr URL | '?vid=' Integer› ]⁺"
    echo "Requires: rtmpdump, wget."
    exit 1
}
[[ $# -lt 2 ]] && usage

print () {
    #return # Sets verbose off.
    echo -ne "\033[01;34m$1"
    shift 1
    echo -e " :: \033[00m\033[01;01m$*\033[00m"
}

trim_sides() {
    [[ $# -ne 3 ]] && exit 1
    overhead=$1
    overfoot=$3
    word=$2
    echo ${word:${#overhead}:$((- ${#overhead} + ${#word} - ${#overfoot}))}
}

UA="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11"

D=$1

until [ "$2" = '' ]; do
    echo $2 | grep -Eo '^[0-9]+$' 1>/dev/null
    if [ $? -eq 0 ]; then vid=$2
    else
        echo $2 | grep -Eo 'vid=[0-9]+' 1>/dev/null
        if [ $? -eq 0 ]; then
            vid=$( echo $2 | grep -Eo 'vid=[0-9]+' | sed 's/vid=//' )
        else usage
        fi
    fi

    F="${D}/tmp_${vid}.xml"
    print "Destination Folder" $D

    RESTfulAPI=http://webservice.canal-plus.com/rest/bigplayer/getVideos/$vid
    wget --quiet -O $F --user-agent="${UA}" $RESTfulAPI
    print "RESTful API" $RESTfulAPI

    if [ ! -f $F ]; then
        echo "Pas pu accéder à la vidéo"
        echo "…ou alors l'ordre des arguments est mauvais"
        usage
    fi

    url=$( cat $F | grep -Eo '<HD>[^<>]+</HD>' )
    url=$( trim_sides '<HD>' "$url" '</HD>' )
    print "Stream Remote Address" $url

    name=$( echo "${url}" | grep -Eo '[^/]+$' )
    name=${name:0:$((${#name} - 4))}

    rm $F
    if [ "${url:0:4}" = 'rtmp' ]; then
        fn="${D}/${name} - Canal+.fr".flv
        rtmpdump -r "${url}" -o "$fn" # --quiet
    else
        fn="${D}/${name} - Canal+.fr".mp4
        wget "$url" -O "$fn"
    fi
    print "File Saved at" "$fn"
    echo -e "$vid\t$(date)\t$url" >> ~/.dled

    shift
done

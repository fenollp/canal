#Canal+.fr videos backup
A UNIX Shell script that accepts command-line requests to download
`www.canalplus.fr`'s videos.

##Synopsis

    :::bash
    ./canal+.sh  ‹destinating folder› ‹Canal+.fr URL | '?vid=' Integer› [ ‹Canal+.fr URL | '?vid=' Integer› ]⁺

###Case Study

Here you have a show you would like to backup, pick its address. It should look
like `http://www.canalplus.fr/c-infos-documentaires/pid1830-c-zapping.html?vid=799800`.

Download the show with:

    :::bash
    ./canal+.sh ~/Downloads http://www.canalplus.fr/c-infos-documentaires/pid1830-c-zapping.html?vid=799800

Or having extracted the vid variable:

    :::bash
    ./canal+.sh ~/Downloads 799800


##Requirements
* rtmpdump (known to work with 2.3)
* wget
